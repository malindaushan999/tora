/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.30-0ubuntu0.18.04.1 : Database - TracerTora
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`TracerTora` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `TracerTora`;

/*Table structure for table `ch_keyword_search_terms` */

DROP TABLE IF EXISTS `ch_keyword_search_terms`;

CREATE TABLE `ch_keyword_search_terms` (
  `ch_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(20) DEFAULT NULL,
  `url_id` int(20) DEFAULT NULL,
  `term` varchar(512) DEFAULT NULL,
  `normalized_term` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`ch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=846 DEFAULT CHARSET=latin1;

/*Table structure for table `ch_urls` */

DROP TABLE IF EXISTS `ch_urls`;

CREATE TABLE `ch_urls` (
  `ch_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` int(20) DEFAULT NULL,
  `url` varchar(8192) DEFAULT NULL,
  `title` varchar(2048) DEFAULT NULL,
  `visit_count` int(20) DEFAULT NULL,
  `typed_count` int(20) DEFAULT NULL,
  `last_visit_time` bigint(32) DEFAULT NULL,
  `hidden` int(32) DEFAULT NULL,
  PRIMARY KEY (`ch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4647 DEFAULT CHARSET=latin1;

/*Table structure for table `sync_configuration` */

DROP TABLE IF EXISTS `sync_configuration`;

CREATE TABLE `sync_configuration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `browser_type` smallint(6) DEFAULT NULL,
  `last_synced_url_id` bigint(20) DEFAULT '0',
  `last_synced_keyword_search_id` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_sync_configuration_browser_type_xxbt_browser_type_id` (`browser_type`),
  CONSTRAINT `fk_sync_configuration_browser_type_xxbt_browser_type_id` FOREIGN KEY (`browser_type`) REFERENCES `xxbt_browser_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `xxbt_browser_type` */

DROP TABLE IF EXISTS `xxbt_browser_type`;

CREATE TABLE `xxbt_browser_type` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `xxbt_yesno` */

DROP TABLE IF EXISTS `xxbt_yesno`;

CREATE TABLE `xxbt_yesno` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `description` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
