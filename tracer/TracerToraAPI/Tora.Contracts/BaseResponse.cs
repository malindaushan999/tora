﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tora.Contracts
{
    public class BaseResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public int Type { get; set; }

        public BaseResponse(bool createTestMessage = false)
        {
            if (createTestMessage)
            {
                Code = "10000";
                Type = 3; // Error
                Message = "Test Message";
            }
        }
    }
}
