﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tora.DB;

namespace Tora.Contracts.User
{
    public class AuthResponse : BaseResponse
    {
        public string AuthToken { get; set; }
        public m_user UserInfo { get; set; } = new m_user();
    }
}
