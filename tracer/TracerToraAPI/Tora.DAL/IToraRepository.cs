﻿using Tora.Contracts.User;
using Tora.DB;

namespace Tora.DAL
{
    public interface IToraRepository
    {
        public Task<user?> GetUserById(int id);
        public Task<AuthResponse> Authenticate(AuthRequest req);
    }
}
