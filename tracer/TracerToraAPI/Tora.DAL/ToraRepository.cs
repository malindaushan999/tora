﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Text;
using Tora.API.Common;
using Tora.Contracts.User;
using Tora.DB;
using static Tora.Common.Enums;

namespace Tora.DAL
{
    public class ToraRepository : IToraRepository
    {
        private readonly ILogger<ToraRepository> _logger;
        private readonly ToraMasterContext _dbContext;
        private readonly AppSettings _appSettings;

        public ToraRepository(ILogger<ToraRepository> logger, ToraMasterContext dbContext, IOptions<AppSettings> appSettings)
        {
            _logger = logger;
            _dbContext = dbContext;
            _appSettings = appSettings.Value;
        }

        public async Task<user?> GetUserById(int id)
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            return null;
        }

        public async Task<AuthResponse> Authenticate(AuthRequest req)
        {
            using (_dbContext)
            {
                // Validate user
                m_user user = _dbContext.m_user.SingleOrDefault(x => x.username == req.Username);

                if (user == null)
                {
                    return null;
                }

                // Match passwords
                string reqPassword = Cipher.DecryptString(req.Password, _appSettings.HashKey);
                string dbPassword = Cipher.DecryptString(user.password, _appSettings.HashKey);

                if (reqPassword != dbPassword)
                {
                    return null;
                }

                var token = Helper.GenerateJwtToken(_appSettings.Secret, user.id, user.username);

                // If a record already in 't_login_session', set 'is_active' to 0 (INACTIVE)
                int oldSessionCount = _dbContext.t_login_session.Where(x => x.user_id == user.id)
                    .Where(x => x.is_active == (short)ACTIVE_FLAG.ACTIVE)
                    .Count();

                if (oldSessionCount > 0)
                {
                    List<t_login_session> loginSessionList = _dbContext.t_login_session.Where(x => x.user_id == user.id)
                        .Where(x => x.is_active == (short)ACTIVE_FLAG.ACTIVE)
                        .ToList();

                    foreach (t_login_session loginSession in loginSessionList)
                    {
                        loginSession.is_active = (short)ACTIVE_FLAG.INACTIVE;
                        loginSession.logged_out_datetime = DateTime.UtcNow;
                    }
                }

                // Add record to t_login_session table
                t_login_session session = new()
                {
                    session_id = token,
                    logged_in_datetime = DateTime.UtcNow,
                    user_id = user.id,
                    is_active = (short)ACTIVE_FLAG.ACTIVE
                };
                _dbContext.t_login_session.Add(session);

                // Save changes to db
                await _dbContext.SaveChangesAsync();

                AuthResponse response = new()
                {
                    AuthToken = token,
                    Code = "10000",
                    Message = "Authentication success.",
                    Type = (short)ERROR_MSG_TYPE.SUCCESS,
                    UserInfo = user
                };

                return response;
            }
        }
    }
}
