﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class login_session
    {
        public int id { get; set; }
        public string session_id { get; set; }
        public DateTime logged_in_datetime { get; set; }
        public long user_id { get; set; }
        public int is_active { get; set; }
        public DateTime? logged_out_datetime { get; set; }
    }
}
