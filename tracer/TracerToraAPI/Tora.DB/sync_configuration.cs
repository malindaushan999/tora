﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class sync_configuration
    {
        public long id { get; set; }
        public short? browser_type { get; set; }
        public long? last_synced_url_id { get; set; }
        public long? last_synced_keyword_search_id { get; set; }

        public virtual xxbt_browser_type browser_typeNavigation { get; set; }
    }
}
