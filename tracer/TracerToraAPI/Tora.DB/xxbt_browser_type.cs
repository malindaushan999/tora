﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class xxbt_browser_type
    {
        public xxbt_browser_type()
        {
            c_sync_configuration = new HashSet<c_sync_configuration>();
        }

        public short id { get; set; }
        public string description { get; set; }

        public virtual ICollection<c_sync_configuration> c_sync_configuration { get; set; }
    }
}
