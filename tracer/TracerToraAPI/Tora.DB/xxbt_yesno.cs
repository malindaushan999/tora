﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class xxbt_yesno
    {
        public short id { get; set; }
        public string description { get; set; }
    }
}
