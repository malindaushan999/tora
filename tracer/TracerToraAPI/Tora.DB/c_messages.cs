﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class c_messages
    {
        public long id { get; set; }
        public string message { get; set; }
        public int? type { get; set; }
    }
}
