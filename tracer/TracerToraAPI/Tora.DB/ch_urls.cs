﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class ch_urls
    {
        public long ch_id { get; set; }
        public int? id { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public int? visit_count { get; set; }
        public int? typed_count { get; set; }
        public long? last_visit_time { get; set; }
        public int? hidden { get; set; }
    }
}
