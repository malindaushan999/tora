﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Tora.DB
{
    public partial class ToraMasterContext : DbContext
    {
        public ToraMasterContext()
        {
        }

        public ToraMasterContext(DbContextOptions<ToraMasterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<c_messages> c_messages { get; set; }
        public virtual DbSet<c_sync_configuration> c_sync_configuration { get; set; }
        public virtual DbSet<ch_keyword_search_terms> ch_keyword_search_terms { get; set; }
        public virtual DbSet<ch_urls> ch_urls { get; set; }
        public virtual DbSet<m_user> m_user { get; set; }
        public virtual DbSet<t_login_session> t_login_session { get; set; }
        public virtual DbSet<xxbt_browser_type> xxbt_browser_type { get; set; }
        public virtual DbSet<xxbt_message_type> xxbt_message_type { get; set; }
        public virtual DbSet<xxbt_yesno> xxbt_yesno { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                throw new Exception();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<c_messages>(entity =>
            {
                entity.Property(e => e.message)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<c_sync_configuration>(entity =>
            {
                entity.HasIndex(e => e.browser_type, "fk_sync_configuration_browser_type_xxbt_browser_type_id");

                entity.Property(e => e.last_synced_keyword_search_id).HasDefaultValueSql("'0'");

                entity.Property(e => e.last_synced_url_id).HasDefaultValueSql("'0'");

                entity.HasOne(d => d.browser_typeNavigation)
                    .WithMany(p => p.c_sync_configuration)
                    .HasForeignKey(d => d.browser_type)
                    .HasConstraintName("fk_sync_configuration_browser_type_xxbt_browser_type_id");
            });

            modelBuilder.Entity<ch_keyword_search_terms>(entity =>
            {
                entity.HasKey(e => e.ch_id)
                    .HasName("PRIMARY");

                entity.Property(e => e.normalized_term).HasMaxLength(512);

                entity.Property(e => e.term).HasMaxLength(512);
            });

            modelBuilder.Entity<ch_urls>(entity =>
            {
                entity.HasKey(e => e.ch_id)
                    .HasName("PRIMARY");

                entity.Property(e => e.title).HasMaxLength(2048);

                entity.Property(e => e.url).HasColumnType("varchar(8192)");
            });

            modelBuilder.Entity<m_user>(entity =>
            {
                entity.Property(e => e.password)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.username)
                    .IsRequired()
                    .HasMaxLength(64);
            });

            modelBuilder.Entity<t_login_session>(entity =>
            {
                entity.Property(e => e.session_id)
                    .IsRequired()
                    .HasMaxLength(300);
            });

            modelBuilder.Entity<xxbt_browser_type>(entity =>
            {
                entity.Property(e => e.description).HasMaxLength(64);
            });

            modelBuilder.Entity<xxbt_message_type>(entity =>
            {
                entity.Property(e => e.description).HasMaxLength(64);
            });

            modelBuilder.Entity<xxbt_yesno>(entity =>
            {
                entity.Property(e => e.description).HasMaxLength(64);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
