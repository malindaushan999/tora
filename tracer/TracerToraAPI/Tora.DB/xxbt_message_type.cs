﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class xxbt_message_type
    {
        public short id { get; set; }
        public string description { get; set; }
    }
}
