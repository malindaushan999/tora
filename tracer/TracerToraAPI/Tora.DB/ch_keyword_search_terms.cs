﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class ch_keyword_search_terms
    {
        public long ch_id { get; set; }
        public int? keyword_id { get; set; }
        public int? url_id { get; set; }
        public string term { get; set; }
        public string normalized_term { get; set; }
    }
}
