﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Tora.DB
{
    public partial class m_user
    {
        public long id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
