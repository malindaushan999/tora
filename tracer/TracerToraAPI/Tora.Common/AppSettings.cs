﻿namespace Tora.API.Common
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string HashKey { get; set; }
        public DatabaseConfig DatabaseConfig { get; set; }
    }

    public class DatabaseConfig
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
