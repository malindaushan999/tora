﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tora.Common
{
    public static class Enums
    {
        public enum ACTIVE_FLAG
        {
            INACTIVE = 0,
            ACTIVE = 1
        }

        public enum ERROR_MSG_TYPE
        {
            ERROR = 0,
            SUCCESS = 1,
            WARNING = 2,
            INFO = 3,
            EXCEPTION = 4
        }
    }
}
