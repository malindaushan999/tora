using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Tora.API.Common
{
    public class Cipher
    {
        // This object is use to lock the password hash when accessing from defferent threads
        private static readonly object passwordHashLocker = new object();

        // Password is encrypted using this hash key
        private static string passwordHash = "";
        public static string PasswordHash
        {
            get
            {
                lock (passwordHashLocker)
                {
                    return passwordHash;
                }
            }
            set
            {
                lock (passwordHashLocker)
                {
                    passwordHash = value;
                }
            }
        }

        /// <summary>
        /// This function is use to create encrypted string
        /// </summary>
        /// <param name="plainText">Text you want to encrypt</param>
        /// <param name="hash">Password hash</param>
        /// <returns>Encrypted string</returns>
        public static string EncryptString(string plainText, string hash)
        {
            // Convert plain text into byte array
            byte[] plainTextBytes = Encoding.Unicode.GetBytes(plainText);
            // Use a memory stream to hold the bytes
            MemoryStream ms = new MemoryStream();
            // Create the key and initialize vector using the password
            TripleDES des = CreateDES(hash);
            // Create the encoder that will write memory stream
            CryptoStream cryptStream = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            cryptStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptStream.FlushFinalBlock();

            ms.Close();
            cryptStream.Close();
            // Return encrypted string
            return Convert.ToBase64String(ms.ToArray());
        }

        /// <summary>
        /// This function is use to decrypted a encrypted string
        /// </summary>
        /// <param name="encryptedText">Encrypted text</param>
        /// <param name="hash">Password hash</param>
        /// <returns>Decrypted string</returns>
        public static string DecryptString(string encryptedText, string hash)
        {
            // Convert encrypted string to byte array
            byte[] encryptedTextBytes = Convert.FromBase64String(encryptedText);
            // Create memory stream to hold the bytes
            MemoryStream ms = new MemoryStream();
            // Create the key and initialize vector using the password
            TripleDES des = CreateDES(hash);
            // Create the decoder that will write memory stream
            CryptoStream decryptStream = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
            decryptStream.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
            decryptStream.FlushFinalBlock();

            ms.Close();
            decryptStream.Close();
            // Return decrypted string
            return Encoding.Unicode.GetString(ms.ToArray());
        }

        /// <summary>
        /// This function is use to create a tripple des object
        /// </summary>
        /// <param name="key">Password hash</param>
        /// <returns>TripleDES object</returns>
        public static TripleDES CreateDES(string key)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            TripleDES des = new TripleDESCryptoServiceProvider
            {
                Key = md5.ComputeHash(Encoding.Unicode.GetBytes(key))
            };
            des.IV = new byte[des.BlockSize / 8];
            return des;
        }
    }
}
