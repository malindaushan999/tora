﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Tora.API.Common
{
    public static class Helper
    {
        /// <summary>
        /// Create database connection string
        /// </summary>
        /// <param name="dbConfig"></param>
        /// <returns></returns>
        public static string CreateConnectionString(DatabaseConfig dbConfig, string hashKey)
        {
            return String.Format("Host={0};Port={1};Database={2};Username={3};Password={4}",
                dbConfig?.Host ?? "localhost",
                dbConfig?.Port ?? "3306",
                Cipher.DecryptString(dbConfig.Database, hashKey),
                Cipher.DecryptString(dbConfig.Username, hashKey),
                Cipher.DecryptString(dbConfig.Password, hashKey));
        }

        /// <summary>
        /// Generate JWT Token
        /// </summary>
        /// <param name="secret">Secret Phrase</param>
        /// <param name="user_id">UserID</param>
        /// <returns></returns>
        public static string GenerateJwtToken(string secret, long user_id, string username)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var claims = new List<Claim>();
            claims.Add(new Claim("id", user_id.ToString()));
            claims.Add(new Claim("username", username));
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(3),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        /// <summary>
        /// Encode Base64 
        /// </summary>
        /// <param name="plainText">Input text</param>
        /// <returns>Encoded string</returns>
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        /// <summary>
        /// Decode Base64
        /// </summary>
        /// <param name="base64EncodedData">Input encoded text</param>
        /// <returns>Decoded string</returns>
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
