﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Tora.API.Common;
using Tora.Contracts;
using Tora.Contracts.User;
using Tora.DAL;
using Tora.DB;
using static Tora.Common.Enums;

namespace Tora.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly AppSettings _appSettings;
        private readonly IToraRepository _repository;

        public UserController(ILogger<UserController> logger, IOptions<AppSettings> appSettings, IToraRepository repository)
        {
            _logger = logger;
            _appSettings = appSettings.Value;
            _repository = repository;
        }

        [HttpPost]
        [Route("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthRequest request)
        {
            try
            {
                if (string.IsNullOrEmpty(request.Username))
                {
                    BaseResponse response = new()
                    {
                        Code = "10001",
                        Type = (short)ERROR_MSG_TYPE.ERROR,
                        Message = "Username is missing in the request."
                    };
                    return BadRequest(response);
                }

                if (string.IsNullOrEmpty(request.Password))
                {
                    BaseResponse response = new()
                    {
                        Code = "10001",
                        Type = (short)ERROR_MSG_TYPE.ERROR,
                        Message = "Password is missing in the request."
                    };
                    return BadRequest(response);
                }

                AuthResponse result = await _repository.Authenticate(request);
                if (result == null)
                {
                    BaseResponse response = new()
                    {
                        Code = "10001",
                        Type = (short)ERROR_MSG_TYPE.ERROR,
                        Message = "Authentication failed."
                    };
                    return BadRequest(response);
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                BaseResponse response = new()
                {
                    Code = "10001",
                    Type = (short)ERROR_MSG_TYPE.EXCEPTION,
                    Message = String.Format("Exception occurred.{0}", ex.Message)
                };
                return BadRequest(response);
            }
        }
    }
}
