using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Fluent;
using TracerTora.Common;
using TracerTora.Database;
using TracerTora.Database.TracerTora;

namespace TracerTora
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureLogging((hostContext, logging) =>
                {
                    logging.ClearProviders();
                    logging.AddConfiguration(hostContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                    logging.AddNLog();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddDbContext<TracerToraContext>(options => 
                        options.UseMySQL(hostContext.Configuration.GetConnectionString("TracerToraConnection"))
                    );
                    services.AddTransient<ISQLiteDatabaseHelper, SQLiteDatabaseHelper>();
                    services.Configure<AppSettings>(hostContext.Configuration.GetSection("AppSettings"));
                    services.AddHostedService<Worker>();
                });
    }
}
