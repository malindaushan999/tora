﻿using System;
using System.Collections.Generic;

namespace TracerTora.Database.TracerTora
{
    public partial class ChUrls
    {
        public long ChId { get; set; }
        public int? Id { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public int? VisitCount { get; set; }
        public int? TypedCount { get; set; }
        public long? LastVisitTime { get; set; }
        public int? Hidden { get; set; }
    }
}
