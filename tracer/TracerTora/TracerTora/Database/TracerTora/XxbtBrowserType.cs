﻿using System;
using System.Collections.Generic;

namespace TracerTora.Database.TracerTora
{
    public partial class XxbtBrowserType
    {
        public XxbtBrowserType()
        {
            SyncConfiguration = new HashSet<SyncConfiguration>();
        }

        public short Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<SyncConfiguration> SyncConfiguration { get; set; }
    }
}
