﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TracerTora.Database.TracerTora
{
    public partial class TracerToraContext : DbContext
    {
        public TracerToraContext()
        {
        }

        public TracerToraContext(DbContextOptions<TracerToraContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ChKeywordSearchTerms> ChKeywordSearchTerms { get; set; }
        public virtual DbSet<ChUrls> ChUrls { get; set; }
        public virtual DbSet<SyncConfiguration> SyncConfiguration { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<XxbtBrowserType> XxbtBrowserType { get; set; }
        public virtual DbSet<XxbtYesno> XxbtYesno { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL("server=172.18.81.76;port=3306;user=tora;password=p@55w0rd;database=TracerTora");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChKeywordSearchTerms>(entity =>
            {
                entity.HasKey(e => e.ChId)
                    .HasName("PRIMARY");

                entity.ToTable("ch_keyword_search_terms");

                entity.Property(e => e.ChId)
                    .HasColumnName("ch_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.KeywordId)
                    .HasColumnName("keyword_id")
                    .HasColumnType("int(20)");

                entity.Property(e => e.NormalizedTerm)
                    .HasColumnName("normalized_term")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.Term)
                    .HasColumnName("term")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.UrlId)
                    .HasColumnName("url_id")
                    .HasColumnType("int(20)");
            });

            modelBuilder.Entity<ChUrls>(entity =>
            {
                entity.HasKey(e => e.ChId)
                    .HasName("PRIMARY");

                entity.ToTable("ch_urls");

                entity.Property(e => e.ChId)
                    .HasColumnName("ch_id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Hidden)
                    .HasColumnName("hidden")
                    .HasColumnType("int(32)");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(20)");

                entity.Property(e => e.LastVisitTime)
                    .HasColumnName("last_visit_time")
                    .HasColumnType("int(32)");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.TypedCount)
                    .HasColumnName("typed_count")
                    .HasColumnType("int(20)");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasMaxLength(8192)
                    .IsUnicode(false);

                entity.Property(e => e.VisitCount)
                    .HasColumnName("visit_count")
                    .HasColumnType("int(20)");
            });

            modelBuilder.Entity<SyncConfiguration>(entity =>
            {
                entity.ToTable("sync_configuration");

                entity.HasIndex(e => e.BrowserType)
                    .HasName("fk_sync_configuration_browser_type_xxbt_browser_type_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.BrowserType)
                    .HasColumnName("browser_type")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.LastSyncedKeywordSearchId)
                    .HasColumnName("last_synced_keyword_search_id")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.LastSyncedUrlId)
                    .HasColumnName("last_synced_url_id")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("'0'");

                entity.HasOne(d => d.BrowserTypeNavigation)
                    .WithMany(p => p.SyncConfiguration)
                    .HasForeignKey(d => d.BrowserType)
                    .HasConstraintName("fk_sync_configuration_browser_type_xxbt_browser_type_id");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XxbtBrowserType>(entity =>
            {
                entity.ToTable("xxbt_browser_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XxbtYesno>(entity =>
            {
                entity.ToTable("xxbt_yesno");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("smallint(6)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(64)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
