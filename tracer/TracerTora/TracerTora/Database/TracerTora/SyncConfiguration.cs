﻿using System;
using System.Collections.Generic;

namespace TracerTora.Database.TracerTora
{
    public partial class SyncConfiguration
    {
        public long Id { get; set; }
        public short? BrowserType { get; set; }
        public long? LastSyncedUrlId { get; set; }
        public long? LastSyncedKeywordSearchId { get; set; }

        public virtual XxbtBrowserType BrowserTypeNavigation { get; set; }
    }
}
