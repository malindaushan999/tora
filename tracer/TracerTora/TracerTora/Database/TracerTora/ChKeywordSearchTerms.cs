﻿using System;
using System.Collections.Generic;

namespace TracerTora.Database.TracerTora
{
    public partial class ChKeywordSearchTerms
    {
        public long ChId { get; set; }
        public int? KeywordId { get; set; }
        public int? UrlId { get; set; }
        public string Term { get; set; }
        public string NormalizedTerm { get; set; }
    }
}
