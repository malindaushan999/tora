﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TracerTora.Database
{
    public interface ISQLiteDatabaseHelper
    {
        string DataSource { get; set; }
        bool CopyToTempFolder();
        bool Sync();
    }
}
