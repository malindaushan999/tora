﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Relational;
using NLog.LayoutRenderers;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TracerTora.Common;
using TracerTora.Database.TracerTora;
using static TracerTora.Common.StaticData;

namespace TracerTora.Database
{
    public class SQLiteDatabaseHelper : ISQLiteDatabaseHelper
    {
        private string _dataSource;
        public string DataSource
        {
            get { return _dataSource; }
            set
            {
                _dataSource = value + "History"; ;
            }
        }

        private readonly SQLiteConnection _connection = null;
        private readonly ILogger<SQLiteDatabaseHelper> _logger;
        public IServiceScopeFactory _serviceScopeFactory;

        public SQLiteDatabaseHelper(ILogger<SQLiteDatabaseHelper> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
            _connection = new SQLiteConnection(@"Data Source=" + Environment.CurrentDirectory + "\\temp\\History");
        }

        public bool CopyToTempFolder()
        {
            _logger.LogDebug("CopyToTempFolder() => [IN]");

            if (_dataSource != null)
            {
                string path = Environment.CurrentDirectory;

                if (!Directory.Exists(path + "\\temp"))
                {
                    Directory.CreateDirectory(path + "\\temp");
                }

                if (File.Exists(_dataSource))
                {
                    File.Copy(_dataSource, path + "\\temp\\History", true);
                    _logger.LogInformation("CopyToTempFolder() => [SUCCESS]");
                    _logger.LogDebug("CopyToTempFolder() => [OUT]");
                    return true;
                }
            }

            _logger.LogDebug("CopyToTempFolder() => [OUT]");
            return false;
        }

        public bool Sync()
        {
            _logger.LogDebug("Sync() => [IN]");

            try
            {
                OpenConnection();

                TableSyncStatus syncStatus = new TableSyncStatus(true);
                CheckSyncStatus(ref syncStatus);

                if (!syncStatus.ChUrls)
                {
                    SyncTable(StaticData.TABLE_URLS, StaticData.COLUMN_ID);
                }

                if (!syncStatus.ChkeywordSearchTerms)
                {
                    SyncTable(StaticData.TABLE_KEYWORD_SEARCH_TERMS, StaticData.COLUMN_KEYWORD_ID);
                }

                CloseConnection();
                _logger.LogInformation("Sync() => [SUCCESS]");
                _logger.LogDebug("Sync() => [OUT]");
                return true;
            }
            catch (Exception ex)
            {
                CloseConnection();
                _logger.LogDebug("Sync() => [FAILED]");
                _logger.LogError("[EXCEPTION] => " + ex.Message);
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                {
                    _logger.LogError("[INNER EXCEPTION] => " + ex.InnerException.Message);
                }
                _logger.LogTrace(BaseHelper.ToJsonString(ex));
                _logger.LogDebug("Sync() => [OUT]");
                return false;
            }
        }

        private void CheckSyncStatus(ref TableSyncStatus syncStatus)
        {
            _logger.LogDebug("CheckSyncStatus() => [IN]");

            using (var scope = _serviceScopeFactory.CreateScope())
            using (TracerToraContext tracerToraContext = scope.ServiceProvider.GetRequiredService<TracerToraContext>())
            {
                int ttLastUrlId = 0;
                int ttLastKeywordSearchId = 0;

                int lastUrlId = GetLastID(StaticData.TABLE_URLS, StaticData.COLUMN_ID);
                int lastKeywordSearchId = GetLastID(StaticData.TABLE_KEYWORD_SEARCH_TERMS, StaticData.COLUMN_KEYWORD_ID);

                if (tracerToraContext.ChUrls.Max(m => m.Id).HasValue)
                {
                    ttLastUrlId = tracerToraContext.ChUrls.Max(m => m.Id).Value;
                }

                if (tracerToraContext.ChKeywordSearchTerms.Max(m => m.KeywordId).HasValue)
                {
                    ttLastKeywordSearchId = tracerToraContext.ChKeywordSearchTerms.Max(m => m.KeywordId).Value;
                }

                if (lastUrlId > ttLastUrlId)
                {
                    syncStatus.ChUrls = false;
                }

                if (lastKeywordSearchId > ttLastKeywordSearchId)
                {
                    syncStatus.ChkeywordSearchTerms = false;
                }
            }

            _logger.LogTrace(BaseHelper.ToJsonString(syncStatus));
            _logger.LogDebug("CheckSyncStatus() => [SUCCESS]");
            _logger.LogDebug("CheckSyncStatus() => [OUT]");
        }

        private bool SyncTable(string table, string column)
        {
            _logger.LogDebug("SyncTable() => [IN]");

            using (IServiceScope scope = _serviceScopeFactory.CreateScope())
            using (TracerToraContext tracerToraContext = scope.ServiceProvider.GetRequiredService<TracerToraContext>())
            using (IDbContextTransaction transaction = tracerToraContext.Database.BeginTransaction())
            {
                int ttLastID = 0;
                switch (table)
                {
                    case StaticData.TABLE_URLS:
                        if (tracerToraContext.ChUrls.Max(m => m.Id).HasValue)
                        {
                            ttLastID = tracerToraContext.ChUrls.Max(m => m.Id).Value;
                        }

                        SQLiteCommand cmd = new SQLiteCommand
                        {
                            Connection = _connection,
                            CommandText = string.Format("SELECT * FROM {0} WHERE {1} > {2}", table, column, ttLastID)
                        };

                        SQLiteDataReader dataReader = cmd.ExecuteReader();

                        List<ChUrls> dataList1 = new List<ChUrls>();

                        while (dataReader.Read())
                        {
                            ChUrls dataObject = new ChUrls
                            {
                                Id = dataReader.GetInt32(0),
                                Url = dataReader.GetString(1),
                                Title = dataReader.GetString(2),
                                VisitCount = dataReader.GetInt32(3),
                                TypedCount = dataReader.GetInt32(4),
                                LastVisitTime = dataReader.GetInt64(5),
                                Hidden = dataReader.GetInt32(6)
                            };

                            dataList1.Add(dataObject);
                        }
                        dataReader.Close();

                        tracerToraContext.ChUrls.AddRange(dataList1);

                        break;
                    case StaticData.TABLE_KEYWORD_SEARCH_TERMS:
                        if (tracerToraContext.ChKeywordSearchTerms.Max(m => m.KeywordId).HasValue)
                        {
                            ttLastID = tracerToraContext.ChKeywordSearchTerms.Max(m => m.KeywordId).Value;
                        }

                        cmd = new SQLiteCommand
                        {
                            Connection = _connection,
                            CommandText = string.Format("SELECT * FROM {0} WHERE {1} > {2}", table, column, ttLastID)
                        };

                        dataReader = cmd.ExecuteReader();

                        List<ChKeywordSearchTerms> dataList2 = new List<ChKeywordSearchTerms>();

                        while (dataReader.Read())
                        {
                            ChKeywordSearchTerms dataObject = new ChKeywordSearchTerms
                            {
                                KeywordId = dataReader.GetInt32(0),
                                UrlId = dataReader.GetInt32(1),
                                Term = dataReader.GetString(2),
                                NormalizedTerm = dataReader.GetString(3)
                            };

                            dataList2.Add(dataObject);
                        }
                        dataReader.Close();

                        tracerToraContext.ChKeywordSearchTerms.AddRange(dataList2);

                        break;
                }

                tracerToraContext.SaveChanges();
                transaction.Commit();
            }

            _logger.LogDebug("SyncTable() => [SUCCESS]");
            _logger.LogDebug("SyncTable() => [OUT]");
            return true;
        }

        private void OpenConnection()
        {
            _logger.LogDebug("OpenConnection() => [IN]");

            if (_connection != null && _connection.State != System.Data.ConnectionState.Open)
            {
                _connection.Open();
                _logger.LogDebug("OpenConnection() => [SUCCESS]");
            }

            _logger.LogDebug("OpenConnection() => [OUT]");
        }

        private void CloseConnection()
        {
            _logger.LogDebug("CloseConnection() => [IN]");

            if (_connection != null)
            {
                _connection.Close();
                _logger.LogInformation("CloseConnection() => [SUCCESS]");
            }

            _logger.LogDebug("CloseConnection() => [OUT]");
        }

        private int GetLastID(string table, string column)
        {
            _logger.LogDebug("GetLastID() => [IN]");

            SQLiteCommand cmd = new SQLiteCommand
            {
                Connection = _connection,
                CommandText = string.Format("SELECT MAX({0}) FROM {1}", column, table)
            };

            int lastID = -1;
            SQLiteDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                lastID = dataReader.GetInt32(0);
            }

            var logMessage = lastID > 0 ? "SUCCESS" : "FAILED";
            _logger.LogInformation(string.Format("GetLastID() => [{0}]", logMessage));
            _logger.LogDebug("GetLastID() => [OUT]");

            return lastID;
        }
    }
}
