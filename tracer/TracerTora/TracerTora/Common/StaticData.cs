﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TracerTora.Common
{
    public static class StaticData
    {
        public const int DEFAULT_SYNC_CYCLE_PERIOD = 1;

        public const string TABLE_URLS = "urls";
        public const string TABLE_KEYWORD_SEARCH_TERMS = "keyword_search_terms";
        public const string COLUMN_ID = "id";
        public const string COLUMN_KEYWORD_ID = "keyword_id";

        public struct TableSyncStatus
        {
            public bool ChUrls { get; set; }
            public bool ChkeywordSearchTerms { get; set; }
            public TableSyncStatus(bool init)
            {
                ChUrls = init;
                ChkeywordSearchTerms = init;
            }
        }
    }
}
