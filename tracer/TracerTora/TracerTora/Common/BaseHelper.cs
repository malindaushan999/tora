﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace TracerTora.Common
{
    public static class BaseHelper
    {
        public static string ToJsonString(object value)
        {
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            return JsonConvert.SerializeObject(value, Newtonsoft.Json.Formatting.Indented, settings);
        }
    }
}
