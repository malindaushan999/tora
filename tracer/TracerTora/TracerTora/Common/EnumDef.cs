﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TracerTora.Common
{
    public class EnumDef
    {
        public enum DBTYPE
        {
            SQLITE,
            MYSQL
        }

        public enum BROWSERTYPE
        { 
            CHROME,
            FIREFOX,
            EDGE,
            IE
        }
    }
}
