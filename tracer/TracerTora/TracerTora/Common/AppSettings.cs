﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TracerTora.Common
{
    public class AppSettings
    {
        public int SyncCyclePeriod { get; set; }
        public SourcePaths SourcePaths { get; set; }
    }

    public class SourcePaths
    {
        public string Chrome { get; set; }
    }
}
