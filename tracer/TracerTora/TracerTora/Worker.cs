using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NLog.LayoutRenderers;
using Org.BouncyCastle.Asn1.Cms;
using TracerTora.Common;
using TracerTora.Database;
using TracerTora.Database.TracerTora;

namespace TracerTora
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly AppSettings _appSettings;
        private readonly ISQLiteDatabaseHelper _dbHelper;
        public Worker(ILogger<Worker> logger, IOptions<AppSettings> appSettings, ISQLiteDatabaseHelper dbHelper)
        {
            _logger = logger;
            _appSettings = appSettings.Value;
            _dbHelper = dbHelper;
            _dbHelper.DataSource = _appSettings.SourcePaths.Chrome;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("TracerTora [START]");

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("SYNC CYCLE [START]");
                int delayTime = (int)TimeSpan.FromMinutes(StaticData.DEFAULT_SYNC_CYCLE_PERIOD).TotalMilliseconds;

                try
                {
                    delayTime = (int)TimeSpan.FromMinutes(_appSettings.SyncCyclePeriod).TotalMilliseconds;
                    if (_dbHelper.CopyToTempFolder())
                    {
                        _dbHelper.Sync();
                    }
                    else
                    {
                        _logger.LogError("CopyToTempFolder() => [FAILED]");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("[EXCEPTION] => " + ex.Message);
                    if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                    {
                        _logger.LogError("[INNER EXCEPTION] => " + ex.InnerException.Message);
                    }
                    _logger.LogTrace(BaseHelper.ToJsonString(ex));
                }

                _logger.LogInformation("SYNC CYCLE [END]");

                await Task.Delay(delayTime, stoppingToken);
            }
        }
    }
}
