﻿using ModuleCommon.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace ModuleCommon
{
    public class ModuleCommonModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RequestNavigate("ContentRegion", nameof(LoginView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<LoginView>();
        }
    }
}