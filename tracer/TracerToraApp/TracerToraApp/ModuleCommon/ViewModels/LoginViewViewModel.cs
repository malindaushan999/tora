﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ModuleCommon.ViewModels
{
    public class LoginViewViewModel : BindableBase, INavigationAware
    {
        protected IRegionManager RegionManager { get; }

        #region Navigation Aware

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }

        #endregion Navigation Aware

        #region Members

        private string m_username;

        public string Username
        {
            get { return m_username; }
            set { m_username = value; }
        }

        private string m_password;

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        public DelegateCommand LoginCommand { get; }

        #endregion Members

        #region Constructor
        public LoginViewViewModel(IRegionManager i_regionManager)
        {
            RegionManager = i_regionManager;

            LoginCommand = new DelegateCommand(LoginExecute);
        }

        #endregion Constructor
        
        #region Methods

        private void LoginExecute()
        {
            
        }

        #endregion Methods
    }
}
